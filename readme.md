# Webform Protected Downloads

### How to use
The module has the following dependencies:   
- webform
- file
- token

Download into modules folder and enable it. Create a new Webform and add a 'Webform Protected Download' handler (*/admin/structure/webform/manage/{test_webform}/handlers*). Set expire time or leave blank for no expiration and check **One time visit link**. Finally upload a file you wish to serve.

There is a **Protected download link** webform submission token available and can be used in form submission messages or sent via email. This takes the form `[webform_submission:protected_download_url]`. You can add multiple handlers for multiple protected downloads. Use sub-tokens for each handler ID to reference each one separately; e.g. `[webform_submission:protected_download_url:webform_protected_downloads_2]`.
