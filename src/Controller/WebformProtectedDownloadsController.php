<?php

namespace Drupal\webform_protected_downloads\Controller;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\file\FileInterface;
use Drupal\webform\Plugin\WebformHandlerInterface;
use Drupal\webform_protected_downloads\WebformProtectedDownloadsManager;
use Drupal\webform_protected_downloads\WebformProtectedDownloadsManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Mime\MimeTypeGuesserInterface;

/**
 * Handles responses for files to be downloaded.
 *
 * @package Drupal\webform_protected_downloads\Controller
 *
 * @phpstan-consistent-constructor
 */
class WebformProtectedDownloadsController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * WebformProtectedDownloadsManager definition.
   *
   * @var \Drupal\webform_protected_downloads\WebformProtectedDownloadsManagerInterface
   */
  protected WebformProtectedDownloadsManagerInterface $webformProtectedDownloadsManager;

  /**
   * The MIME type guesser.
   *
   * @var \Symfony\Component\Mime\MimeTypeGuesserInterface
   */
  protected $mimeTypeGuesser;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * WebformProtectedDownloadsController constructor.
   *
   * @param \Drupal\webform_protected_downloads\WebformProtectedDownloadsManager $webform_protected_downloads_manager
   *   WebformProtectedDownloadsManager definition.
   * @param \Symfony\Component\Mime\MimeTypeGuesserInterface $mime_type_guesser
   *   The MIME type guesser.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   Current user.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(
    WebformProtectedDownloadsManager $webform_protected_downloads_manager,
    MimeTypeGuesserInterface $mime_type_guesser,
    AccountInterface $current_user,
    EntityTypeManager $entity_type_manager,
  ) {
    $this->webformProtectedDownloadsManager = $webform_protected_downloads_manager;
    $this->mimeTypeGuesser = $mime_type_guesser;
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('webform_protected_downloads.manager'),
      $container->get('file.mime_type.guesser'),
      $container->get('current_user'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Protected file download controller.
   *
   * @param string $hash
   *   The unique hash sent by the request.
   *
   * @return \Drupal\Core\Routing\TrustedRedirectResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse|\Symfony\Component\HttpFoundation\RedirectResponse
   *   Either the file to be downloaded or an HTTP error.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function protectedFileDownload(string $hash) {
    $wpd_settings = $expired = NULL;

    // Get corresponding protected file entry from given URL hash.
    if ($webformProtectedDownload = $this->webformProtectedDownloadsManager->getSubmissionByHash($hash)) {
      $expired = ($webformProtectedDownload->expire->value < time() && $webformProtectedDownload->expire->value != "0");
      $protected_downloads_handler = $webformProtectedDownload->getHandler(FALSE);

      if ($protected_downloads_handler instanceof WebformHandlerInterface) {
        $wpd_settings = $protected_downloads_handler->getSettings();
        $is_owner = FALSE;
        $has_access = FALSE;

        if (!empty($wpd_settings['verify_access']) && $wpd_settings['verify_access'] !== 'basic') {
          if (in_array($wpd_settings['verify_access'], [
            'owner',
            'owner_or_view_submission',
            'owner_and_view_submission',
          ])) {
            $is_owner = $webformProtectedDownload->getWebformSubmission()->isOwner($this->currentUser);
          }

          if (in_array($wpd_settings['verify_access'], [
            'view_submission',
            'owner_or_view_submission',
            'owner_and_view_submission',
          ])) {
            $has_access = $webformProtectedDownload->getWebformSubmission()->access('view');
          }

          switch ($wpd_settings['verify_access']) {
            case 'owner':
              if (!$is_owner) {
                $expired = TRUE;
              }
              break;

            case 'view_submission':
              if (!$has_access) {
                $expired = TRUE;
              }
              break;

            case 'owner_and_view_submission':
              if (!$has_access || !$is_owner) {
                $expired = TRUE;
              }
              break;

            case 'owner_or_view_submission':
              if (!$has_access && !$is_owner) {
                $expired = TRUE;
              }
              break;
          }
        }
      }
    }

    // Return error page if no results, inactive, expired, no webform found
    // or file not found.
    if (
      !$wpd_settings
      || !$webformProtectedDownload
      || !$webformProtectedDownload->isActive()
      || $expired
      || !$webformProtectedDownload->file->target_id
      || $webformProtectedDownload->file->target_id != current($wpd_settings['protected_file'])
    ) {
      if (!empty($wpd_settings['expiration_error_message'])) {
        $this->messenger()->addError($wpd_settings['expiration_error_message']);
      }

      switch ($wpd_settings['expiration_page'] ?? NULL) {
        case "homepage":
          return $this->redirect('<front>');

        case "page_reload":
          if ($webformProtectedDownload) {
            return new RedirectResponse($webformProtectedDownload->getWebformSubmission()->getWebform()->toUrl()->setAbsolute()->toString());
          }
          else {
            throw new NotFoundHttpException();
          }

        case "custom":
          if (isset($wpd_settings['expiration_page_custom'])) {
            $path = $wpd_settings['expiration_page_custom'];
            if (UrlHelper::isExternal($path)) {
              return new TrustedRedirectResponse(Url::fromUri($path)->setAbsolute()->toString());
            }
            else {
              $path = '/' . ltrim($path, '/');
              return new RedirectResponse(Url::fromUserInput($path)->setAbsolute()->toString());
            }
          }
          // If the custom link page setting is missing, just show a 404.
          else {
            throw new NotFoundHttpException();
          }

        default:
          throw new NotFoundHttpException();
      }
    }

    // Get file response.
    $response = $this->sendProtectedFileResponse($webformProtectedDownload->file->target_id);

    // Set onetime entry to inactive before returning.
    if ($webformProtectedDownload->isOneTimeLink()) {
      $webformProtectedDownload->set('active', FALSE)->save();
    }

    return $response;
  }

  /**
   * Gets the file from fid and creates a download http response.
   */
  private function sendProtectedFileResponse($fid): BinaryFileResponse {

    // Get all the needed parameters.
    $file = $this->entityTypeManager->getStorage('file')->load($fid);
    if (!$file instanceof FileInterface) {
      throw new NotFoundHttpException();
    }
    $uri = $file->getFileUri();

    // Set HTTP header parameters.
    $headers = [
      'Content-Type' => $this->mimeTypeGuesser->guessMimeType($uri) . '; name="' . iconv_mime_decode(basename($uri)) . '"',
      'Content-Length' => filesize($uri),
      'Content-Disposition' => 'attachment; filename="' . iconv_mime_decode($file->getFilename()) . '"',
      'Cache-Control' => 'private',
    ];
    return new BinaryFileResponse($uri, 200, $headers);
  }

}
