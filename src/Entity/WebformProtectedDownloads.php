<?php

namespace Drupal\webform_protected_downloads\Entity;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Url;
use Drupal\webform\Plugin\WebformHandlerInterface;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Defines the Webform protected downloads entity.
 *
 * @ingroup webform_protected_downloads
 *
 * @ContentEntityType(
 *   id = "webform_protected_downloads",
 *   label = @Translation("Webform protected downloads"),
 *   base_table = "webform_protected_downloads",
 *   entity_keys = {
 *     "id" = "id",
 *   },
 * )
 */
class WebformProtectedDownloads extends ContentEntityBase {

  /**
   * Return WebformSubmission.
   *
   * @return \Drupal\webform\WebformSubmissionInterface|null
   *   Return WebformSubmission.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getWebformSubmission(): ?WebformSubmissionInterface {
    if ($first = $this->get('webform_submission')->first()) {
      /** @var \Drupal\Core\Entity\Plugin\DataType\EntityReference $reference */
      $reference = $first->get('entity');
      return $reference->getValue();
    }
    return NULL;
  }

  /**
   * Get hash.
   *
   * @return null|string
   *   Return hash if exists.
   */
  public function getHash(): ?string {
    return $this->hash->value;
  }

  /**
   * Get webform handler.
   *
   * @param bool $verify_file
   *   Verify the webform handler still references the same file entity as the
   *   protected download entity. Defaults to TRUE.
   *
   * @return \Drupal\webform\Plugin\WebformHandlerInterface|null
   *   Return handler if exists and enabled.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getHandler(bool $verify_file = TRUE): ?WebformHandlerInterface {
    // Get WebformSubmission.
    $webformSubmission = $this->getWebformSubmission();
    if ($webformSubmission instanceof WebformSubmissionInterface) {
      // Check handler is active.
      if ($webform = $webformSubmission->getWebform()) {
        try {
          $handler = $webform->getHandler($this->handler_id->value);
          if ($handler->isEnabled()) {
            // Check that the handler still references the same file, unless
            // that verification step can be skipped.
            if (!$verify_file || $this->file->target_id == current($handler->getSetting('protected_file'))) {
              return $handler;
            }
          }
        }
        catch (PluginNotFoundException $e) {
          // Referenced handler is no longer available.
        }
      }
    }
    return NULL;
  }

  /**
   * Check if link is active.
   *
   * @return bool
   *   Return bool.
   */
  public function isActive(): bool {
    return (bool) $this->active->value;
  }

  /**
   * Check if link is only one time usable.
   *
   * @return bool
   *   Return bool.
   */
  public function isOneTimeLink(): bool {
    return (bool) $this->onetime->value;
  }

  /**
   * Returns the link URL for downloading this protected download.
   *
   * @return \Drupal\Core\GeneratedUrl|string|null
   *   The link URL.
   */
  public function getUrl() {
    $hash = $this->getHash();
    if (!is_null($hash)) {
      $url = Url::fromRoute('webform_protected_downloads.download', [
        'hash' => $hash,
      ], [
        'absolute' => TRUE,
      ]);
      return $url->toString();
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['webform_submission'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Webform Submission'))
      ->setSetting('target_type', 'webform_submission')
      ->setRequired(TRUE);

    $fields['handler_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Handler ID'))
      ->setRequired(TRUE);

    $fields['file'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('File'))
      ->setSetting('target_type', 'file')
      ->setRequired(TRUE)
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);

    $fields['hash'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Hash'))
      ->setRequired(TRUE);

    $fields['active'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Active'))
      ->setDefaultValue(FALSE);

    $fields['expire'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Expire'))
      ->setRequired(TRUE);

    $fields['onetime'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Onetime'))
      ->setDefaultValue(FALSE)
      ->setRequired(TRUE);

    return $fields;
  }

}
