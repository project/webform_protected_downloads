<?php

namespace Drupal\webform_protected_downloads\Plugin\WebformHandler;

use Drupal\Component\Render\PlainTextOutput;
use Drupal\Component\Utility\DeprecationHelper;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\ElementInfoManagerInterface;
use Drupal\file\FileUsage\FileUsageInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Handler for protected downloads.
 *
 * @WebformHandler(
 *   id = "webform_protected_downloads",
 *   label = @Translation("Webform Protected Download"),
 *   category = @Translation("Downloads"),
 *   description = @Translation("Provides an access controlled link to a file/page of files."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_IGNORED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_REQUIRED,
 * )
 */
class WebformProtectedDownloadsHandler extends WebformHandlerBase {

  /**
   * The file storage handler.
   *
   * @var \Drupal\file\FileStorageInterface
   */
  protected $fileStorage;

  /**
   * The file usage service.
   *
   * @var \Drupal\file\FileUsage\FileUsageInterface
   */
  protected FileUsageInterface $fileUsage;

  /**
   * The element info manager.
   *
   * @var \Drupal\Core\Render\ElementInfoManagerInterface
   */
  protected ElementInfoManagerInterface $elementInfoManager;

  /**
   * The file validator service.
   *
   * @var \Drupal\file\Validation\FileValidatorInterface|null
   */
  protected $fileValidator;

  /**
   * {@inheritdoc}
   */
  final public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $entityTypeManager = $container->get('entity_type.manager');
    $instance->fileStorage = $entityTypeManager->getStorage('file');
    $instance->fileUsage = $container->get('file.usage');
    $instance->elementInfoManager = $container->get('plugin.manager.element_info');
    $instance->fileValidator = $container->get('file.validator', ContainerInterface::NULL_ON_INVALID_REFERENCE);

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $extensions = $this->configFactory->get('webform.settings')
      ->get('file.default_managed_file_extensions') ?? '';

    return [
      'verify_access' => 'basic',
      'expiration_one_time' => FALSE,
      'expiration_time' => 0,
      'expiration_page' => 'page_reload',
      'expiration_page_custom' => '',
      'expiration_error_message' => 'This link has expired.',
      'protected_file' => [],
      'protected_file_allowed_extensions' => $extensions,
      'debug' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $configuration = $this->getConfiguration();
    $settings = $configuration['settings'];

    return [
      '#settings' => $settings,
    ] + parent::getSummary();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['validity'] = [
      '#type' => 'details',
      '#title' => $this->t('Validity'),
      '#open' => TRUE,
    ];
    $form['validity']['verify_access'] = [
      '#type' => 'radios',
      '#title' => $this->t('Verify download links'),
      '#options' => [
        'basic' => $this->t('Basic'),
        'owner' => $this->t('Require the current user to have made the submission'),
        'view_submission' => $this->t('Require access to view submission'),
        'owner_or_view_submission' => $this->t('Require <strong>either</strong> the current user to have made the submission, <strong>or</strong> that they have access to view submission'),
        'owner_and_view_submission' => $this->t('Require <strong>both</strong> the current user to have made the submission, <strong>and</strong> that they have access to view submission'),
      ],
      '#default_value' => $this->getSetting('verify_access') ?? NULL,
      '#description' => $this->t('Links are generated uniquely for every submission. The hash in a link and any expiry conditions are verified regardless of which option is chosen. Requiring the current user to have made the submission stops users from being able to share their unique links with others. Access to view the submission is often only granted to administrators, but is <a href=":url">configurable</a>.', [
        ':url' => $this->getWebform()->toUrl('settings-access')->toString(),
      ]),
      '#description_display' => 'before',
      '#help' => FALSE,
    ];
    $form['validity']['expiration_one_time'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('One time visit link'),
      '#default_value' => $this->getSetting('expiration_one_time'),
    ];
    $form['validity']['expiration_time'] = [
      '#type' => 'number',
      '#title' => $this->t('Expire after X minutes'),
      '#default_value' => $this->getSetting('expiration_time'),
      '#required' => TRUE,
      '#description' => $this->t('Set this to 0 for unlimited.'),
    ];
    $options = [
      '404' => $this->t('404 page'),
      'homepage' => $this->t('Homepage with error message'),
      'page_reload' => $this->t('Form page with error message'),
      'custom' => $this->t('Custom page'),
    ];
    $form['validity']['expiration_page'] = [
      '#type' => 'radios',
      '#title' => $this->t('Link expired page'),
      '#required' => TRUE,
      '#description' => $this->t('Select a page to be routed when link expires.'),
      '#options' => $options,
      '#default_value' => $this->getSetting('expiration_page'),
      '#attributes' => [
        'data-wpd-selector' => 'expiration-page',
      ],
    ];
    $form['validity']['expiration_page_custom'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom link page'),
      '#default_value' => $this->getSetting('expiration_page_custom'),
      '#states' => [
        'visible' => [
          ':input[data-wpd-selector="expiration-page"]' => [
            'value' => 'custom',
          ],
        ],
        'required' => [
          ':input[data-wpd-selector="expiration-page"]' => [
            'value' => 'custom',
          ],
        ],
      ],
    ];
    $form['validity']['expiration_error_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Error message'),
      '#description' => $this->t('Error message to display.'),
      '#default_value' => $this->getSetting('expiration_error_message'),
      '#states' => [
        'visible' => [
          ':input[data-wpd-selector="expiration-page"]' => [
            ['value' => 'custom'],
            ['value' => 'homepage'],
            ['value' => 'page_reload'],
          ],
        ],
        'required' => [
          ':input[data-wpd-selector="expiration-page"]' => [
            ['value' => 'custom'],
            ['value' => 'homepage'],
            ['value' => 'page_reload'],
          ],
        ],
      ],
    ];

    // Files.
    $form['protected'] = [
      '#type' => 'details',
      '#title' => $this->t('Protected Files'),
      '#open' => TRUE,
      '#id' => Html::getId('webform_protected_downloads-wrapper'),
    ];
    $form['protected']['protected_file_allowed_extensions'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Valid File extensions'),
      '#description' => $this->t("Separate extensions with a space."),
      '#default_value' => $this->getSetting('protected_file_allowed_extensions'),
      '#required' => TRUE,
      '#maxlength' => 196,
      '#element_validate' => [[$this, 'allowedExtensionsElementValidate']],
      '#limit_validation_errors' => [array_merge($form['#parents'], ['protected_file_allowed_extensions'])],
      '#ajax' => [
        'callback' => [static::class, 'allowedExtensionsAjaxCallback'],
        'disable-refocus' => TRUE,
        'wrapper' => $form['protected']['#id'],
      ],
    ];
    $form['protected']['protected_file'] = [
      '#name' => 'protected_file',
      '#type' => 'managed_file',
      '#title' => $this->t('Choose file for protected download'),
      '#multiple' => FALSE,
      '#upload_validators' => [],
      '#upload_location' => 'private://webform_protected_downloads/' . PlainTextOutput::renderFromHtml($this->replaceTokens('[date:custom:Y]-[date:custom:m]')),
      '#default_value' => $this->getSetting('protected_file'),
    ];

    // Add whichever file extensions validator is available.
    $allowed_extensions = $form_state->getValue('protected_file_allowed_extensions', $this->getSetting('protected_file_allowed_extensions'));
    if (class_exists(DeprecationHelper::class)) {
      $form['protected']['protected_file']['#upload_validators'] += DeprecationHelper::backwardsCompatibleCall(
        \Drupal::VERSION,
        '10.2',
        static fn () => [
          'FileExtension' => ['extensions' => $allowed_extensions],
        ],
        static fn () => [
          'file_validate_extensions' => [$allowed_extensions],
        ]
      );
    }
    else {
      $form['protected']['protected_file']['#upload_validators']['file_validate_extensions'] = [$allowed_extensions];
    }

    // Development.
    $form['development'] = [
      '#type' => 'details',
      '#title' => $this->t('Development settings'),
      '#open' => TRUE,
    ];
    $form['development']['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable debugging'),
      '#description' => $this->t('If checked, debugging will be displayed onscreen to all users.'),
      '#return_value' => TRUE,
      '#default_value' => $this->getSetting('debug'),
    ];

    return $this->setSettingsParents($form);
  }

  /**
   * Element validation callback for the allowed extensions element.
   *
   * Requires already-uploaded files to be removed before their extensions can
   * be disallowed.
   *
   * @see \Drupal\file\Validation\FileValidator::validateExtensions()
   */
  public function allowedExtensionsElementValidate(&$element, FormStateInterface $form_state, &$complete_form) {
    // Check which file extensions are already in use by uploads.
    $parents = $element['#parents'];
    array_pop($parents);
    $parents[] = 'protected_file';
    $parents[] = 'fids';
    $uploaded = $form_state->getValue($parents, []);

    if (class_exists(DeprecationHelper::class)) {
      $ideal_callable = [$this->fileValidator, 'validate'];
      [$validator_callable, $validator_args] = DeprecationHelper::backwardsCompatibleCall(
        \Drupal::VERSION,
        '10.2',
        static fn () => [
          $ideal_callable,
          ['FileExtension' => ['extensions' => $element['#value']]],
        ],
        static fn () => [
          'file_validate',
          ['file_validate_extensions' => [$element['#value']]],
        ]
      );
    }
    else {
      $validator_callable = 'file_validate';
      $validator_args = ['file_validate_extensions' => [$element['#value']]];
    }

    /** @var \Drupal\file\FileInterface $file */
    foreach ($this->fileStorage->loadMultiple($uploaded) as $file) {
      $validation_errors = $validator_callable($file, $validator_args);

      if (count($validation_errors) > 0) {
        $subject = basename($file->isTemporary() ? $file->getFilename() : $file->getFileUri());
        preg_match('/\.([^ ]+)$/i', $subject, $matches);
        $form_state->setError($element, $this->t('The extension %extension is needed for the file %filename which has already been uploaded. Include that extension or remove the file.', [
          '%filename' => $subject,
          '%extension' => $matches[1],
        ]));
      }
    }
  }

  /**
   * AJAX callback to update the allowed extensions for the upload element.
   *
   * Updates the whole 'protected' details element that contains both the
   * allowed extensions element, and the upload element.
   */
  public static function allowedExtensionsAjaxCallback(array &$form, FormStateInterface $form_state) {
    $parents = $form_state->getTriggeringElement()['#array_parents'];
    array_pop($parents);
    return NestedArray::getValue($form, $parents);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Allow files that are no longer used to be deleted.
    if ($existing_files = $this->getSetting('protected_file')) {
      /** @var \Drupal\File\FileInterface $file */
      foreach ($this->fileStorage->loadMultiple($existing_files) as $file) {
        $this->fileUsage->delete($file, 'webform_protected_downloads', 'webform', $this->getWebform()->id());
      }
    }

    $this->configuration = $this->defaultConfiguration();
    parent::submitConfigurationForm($form, $form_state);
    $this->applyFormStateToConfiguration($form_state);

    // Set the files as permanent through file usage.
    if ($protectedFiles = $form_state->getValue('protected_file')) {
      foreach ($this->fileStorage->loadMultiple($protectedFiles) as $file) {
        $this->fileUsage->add($file, 'webform_protected_downloads', 'webform', $this->getWebform()->id());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function hasAnonymousSubmissionTracking() {
    return in_array($this->getSetting('verify_access'), [
      'owner',
      'owner_or_view_submission',
      'owner_and_view_submission',
    ], TRUE);
  }

}
