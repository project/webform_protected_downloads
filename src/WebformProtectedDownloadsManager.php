<?php

namespace Drupal\webform_protected_downloads;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Manages the retrieval of submissions for webform_protected_downloads.
 *
 * @package Drupal\webform_protected_downloads
 */
class WebformProtectedDownloadsManager implements WebformProtectedDownloadsManagerInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a GroupContentCardinalityValidator object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getSubmissionByUuid(string $uuid, string $handler_id = '') {
    $storage = $this->entityTypeManager->getStorage('webform_protected_downloads');
    $query = $storage->getQuery()
      ->accessCheck(TRUE)
      ->condition('webform_submission.entity.uuid', $uuid);
    if (!empty($handler_id)) {
      $query->condition('handler_id', $handler_id);
    }

    foreach ($storage->loadMultiple($query->execute()) as $entity) {
      if ($entity->getHandler()) {
        // Return WebformProtectedDownload.
        return $entity;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getSubmissionByHash(string $hash) {
    $storage = $this->entityTypeManager->getStorage('webform_protected_downloads');
    $result = $storage->getQuery()
      ->accessCheck(TRUE)
      ->condition('hash', $hash)
      ->execute();

    // Return entity if exists or FALSE.
    if ($result) {
      return $storage->load(reset($result));
    }
    else {
      return FALSE;
    }
  }

}
