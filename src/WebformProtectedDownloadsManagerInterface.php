<?php

namespace Drupal\webform_protected_downloads;

/**
 * Provides an interface for the webform_protected_downloads.manager service.
 *
 * @package Drupal\webform_protected_downloads
 */
interface WebformProtectedDownloadsManagerInterface {

  /**
   * Function will search submission by uuid.
   *
   * @param string $uuid
   *   Uuid of webform submission.
   * @param string $handler_id
   *   Optionally filter by handler ID. Empty strings will be ignored.
   *
   * @return \Drupal\webform_protected_downloads\Entity\WebformProtectedDownloads|false
   *   Return entity or FALSE.
   */
  public function getSubmissionByUuid(string $uuid, string $handler_id = '');

  /**
   * Get WebformProtectedDownloads entity by hash.
   *
   * @param string $hash
   *   The hash.
   *
   * @return \Drupal\webform_protected_downloads\Entity\WebformProtectedDownloads|false
   *   Return entity or false.
   */
  public function getSubmissionByHash(string $hash);

}
